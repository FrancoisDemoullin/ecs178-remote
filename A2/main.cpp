#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>
#include <vector>

#include "global.h"
#include "geometry.h"
#include "polygon.h"
#include "intersection.h"

using namespace Geometry;

int global::gScreenWidth = 800;
int global::gScreenHeight = 800;
float global::gAreaCutoff = 200; // 16
float global::gT = 0.5;
myPoly global::gCurPoly;

std::vector<myPoly> global::gPolys;

void myInit() 
{
    glClearColor(0.0,0.0,0.0,0.0);
    glColor3f(1.0,0.0,0.0);
    glPointSize(4.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,800.0,0.0,800.0);
}

void myMouse(int button, int state, int x, int y) 
{
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
      global::drawDot(x, global::gScreenHeight - y);

      global::gCurPoly.addPoint(Point(x, global::gScreenHeight - y));
      cout << "Point has been added" << endl; 
    }
}

void myDisplay() 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}

void userInput(unsigned char key, int x, int y)
{
	int currentNumberOfPolys = global::gPolys.size();

	// debugging only
	vector<Geometry::Point> a = {Point(100, 100), Point(150, 100), Point(150, 150)};
	vector<Geometry::Point> b = {Point(100, 100), Point(150, 100), Point(150, 150)};
	myPoly pA = myPoly(a);
	myPoly pB = myPoly(b);

	switch(key) 
	{
	case 'a':
		cout << "adding the current polygon" << endl;
		global::gPolys.push_back(global::gCurPoly);
		// reset the current poly
		global::gCurPoly = myPoly();
		break;

	case 'c':
		cout << "addPoint" << endl;
		int lIndex;
		int lPosition;
		cout << "Which Polygon do you want to add this point to? Type the polygon index: ";
		cin >> lIndex;
		cout << "Type the position within the control polygon to add the point: ";
		cin >> lPosition; 
		global::gPolys[lIndex].addPoint(global::gCurPoly.mControlPoints[global::gCurPoly.mControlPoints.size() - 1], lPosition);

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glFlush();
	// no break!

	case 'd':
		cout << "complete DeCasteljau draw has been initiated" << endl;
		for (auto &cur : global::gPolys)
		{
		 	cur.drawControlPoly();
		 	cur.drawBezier(0.001);
		}
		break;
	case 'r':
		cout << "we are raising the degree for you" << endl;
		global::gPolys[0].raiseDegree();
		global::gPolys[0].drawControlPoly();
		global::gPolys[0].drawBezier(0.001);
		break;

	case 'l':
		cout << "we are lowering the degree for you" << endl;
		global::gPolys[0].reduceDegree();
		global::gPolys[0].drawControlPoly();
		global::gPolys[0].drawBezier(0.001);
		break;

	case 'f':
		cout << "Aitken computation for one point has started" << endl;
		global::gPolys[0].drawAitkenPoint(global::gT);
		cout << "Aitken computation for one point has ended" << endl;
		break;

	case 'g':
		cout << "Aitken computation for the entire polygon has started" << endl;
		global::gPolys[0].drawAitkenPolygon(0.001);
		cout << "Aitken computation for the entire polygon has ended" << endl;
		break;

	case 'x':
		int lIndex0;
		int lPosition0;
		cout << "Which Polygon do you want to delete a point from? Type the polygon index: ";
		cin >> lIndex0;
		cout << "Type wthe position within the control polygon to delete the point from: ";
		cin >> lPosition0; 
		global::gPolys[lIndex0].deletePoint(lPosition0);

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glFlush();
		for (auto &cur : global::gPolys)
		{
		 	cur.drawControlPoly();
		 	cur.drawBezier(0.001);
		}
		break;

	case 't':
		cout << "Request to change t value, current t value is: " << global::gT << endl;
		cout << "Please type the new value: ";
		cin >> global::gT;
		cout << "New t value is set to: " << global::gT << endl;
		break;

	case 'y':
		cout << "Setting up the polygon with derivatives for you" << endl;
		global::gPolys[0].derivatives();
		global::gPolys[0].drawControlPoly();
		global::gPolys[0].spline();
		break;

	case 'z':
		cout << "Setting up the polygon for you, you did not specify derivative vectors" << endl;
		global::gPolys[0].drawControlPoly();
		global::gPolys[0].spline();
		break;
	default:
		// do nothing
		break;
	}
}

int main(int argc, char *argv[]) 
{
  global::gCurPoly = myPoly();

  cout << "TEST" << endl;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
  glutInitWindowSize(global::gScreenWidth, global::gScreenHeight);
  glutInitWindowPosition(1000, 0);
  glutCreateWindow("ECS178 - A1");

  glutMouseFunc(myMouse);
  glutDisplayFunc(myDisplay);

  glutKeyboardFunc(userInput);

  myInit();
  glutMainLoop();

  return 0;
}