


Francois Demoullin

A2 - ECS178


Compilation:

make
./out

I am using Clang as the compiler for this project, change line 1 of the makefile from clang to g++ (CC=g++) if you want the compiler to be gcc. Clang produces nicer error messages and it is heavily used for all of Google's C++ development, which is why I like it.  

Manual:

When compiling you will see a simple black window. 

To add a polygon, start clicking into the window. Red dots will appear at the locations you click. They are the control points for the polygon.
After clicking at the locations you think should be control points hit 'a', this adds the current polygon to the list of polygons in the scene.

User input commands:

a: add the polygon

c: add point to polygon
x: delete point of polygon

d: draw the last added poly using the DeCasteljau algo.
t: change t value

r: raise degree of the 1st polygon that was added
l: lower the degree of the 1st polygon that was added

f: Aitken for one point only fot the first polygon you added! This displays all the line segments too. Make sure to set t.
g: Aitken for all points for the first polygon you added.

y: Polygon with derivatives. Splits up the polygon and draws the spline.
z: Creates its own derivatives, then draws the polygon. 


I apologize, there is no GUI. There are no sliders nor are there any fancy colors. The logic behind the program is what I personally care about the most (GUI, colors and fancy artsy things are tedious to implement and sort of a waste of time). Everything seems to work, I dont know of any bugs which I would list here if I was aware of them.