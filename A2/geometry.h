
#pragma once

#include <stdlib.h>  // abs
#include <math.h>
#include <assert.h>
#include <vector>
#include <algorithm>  // sort


namespace Geometry
{

using namespace std;

struct Point
{
  double x;
  double y;

  float mValue;

  const float EPSILON = 0.001f;

  inline Point()
      : x(0.0), y(0.0), mValue(0.0) {}
  inline Point(int pX, int pY, float pValue)
      : x(pX), y(pY), mValue(pValue) {}
  inline Point(int pX, int pY)
      : x(pX), y(pY), mValue(-1.0) {}
  inline Point(double pX, double pY, float pValue)
      : x(pX), y(pY), mValue(pValue) {}
  inline Point(double pX, double pY)
      : x(pX), y(pY), mValue(-1.0) {}
  inline Point(const Point& pPoint)
  {
      x = pPoint.x;
      y = pPoint.y;
      mValue = pPoint.mValue;
  }
  void setX(int pX) { x = pX; }
  void setY(int pY) { y = pY; }
  void setX(double pX) { x = pX; }
  void setY(double pY) { y = pY; }
  void setValue(float pValue) { mValue = pValue; }
  inline float computeDistance(Point pOther)
  {
      return pow(pow(x - pOther.x, 2) + pow(y - pOther.y, 2), 0.5);
  }
	inline float computeDistanceSquared(Point pOther)
	{
		return (x - pOther.x) * (x - pOther.x) + (y - pOther.y) * (y - pOther.y);
	}
  inline void operator=(const Point& pP)
  {
      x = pP.x;
      y = pP.y;
      mValue = pP.mValue;
  }
  inline Point operator*(const float pF)
  {
      float lX = pF * x;
      float lY = pF * y;
      return Point(lX, lY);
  }
  inline Point operator+(const Point& other)
  {
      float lX = x + other.x;
      float lY = y + other.y;
      return Point(lX, lY);
  }
  inline bool operator==(const Point& other) const
  {
      const float EPSILON = 0.001f;
      if (abs(other.x - x) < EPSILON && abs(other.y - y) < EPSILON)
      {
          return true;
      }
      return false;
  }
};

struct Segment
{
    Point mP0;
    Point mP1;

    const float INTERSECTION_EPSILON = 0.3f;

    inline Segment()
        : mP0(-1.0, -1.0), mP1(-1.0, -1.0) {}
    inline Segment(int pX0, int pY0, int pX1, int pY1)
        : mP0(pX0, pY0), mP1(pX1, pY1) {}
    inline Segment(Point pP0, Point pP1)
        : mP0(pP0), mP1(pP1) {}
    inline void operator=(const Segment& pOther)
    {
        mP0 = pOther.mP0;
        mP1 = pOther.mP1;
    }
    bool isOnLine(float pX, float pY);
    float getSlope();
};

struct Line
{
    Point mP0;
    float mSlope;
    float mB;
    bool isInf;

    inline Line(Point pP, float pSlope)
        : mP0(pP), mSlope(pSlope), isInf(false)
    {
        mB = pP.y - pSlope * pP.x;
    }
    inline Line(Point pP, float pSlope, bool pDegenerate)
        : mP0(pP), mSlope(pSlope), isInf(pDegenerate)
    {
        if (pDegenerate == false)
        {
            mB = pP.y - pSlope * pP.x;
        }
    }
};

	
}