#pragma once

#include <vector>
#include <GL/glut.h>
#include <math.h>

#include "polygon.h"
#include "geometry.h"

namespace global
{
	extern int gScreenHeight;
	extern int gScreenWidth;
	extern myPoly gCurPoly;
	extern std::vector<myPoly> gPolys;
	extern float gAreaCutoff;
	extern float gT;

	inline void drawDot(int x, int y) 
	{
	  glBegin(GL_POINTS);
	  glVertex2i(x,y);

	  glEnd();
	  glFlush();
	}

	inline void drawLine(Geometry::Point p1, Geometry::Point p2) 
	{
	  glBegin(GL_LINES);
	  glVertex3f(p1.x, p1.y, 0.0f);
	  glVertex3f(p2.x, p2.y, 0.0f);

	  glEnd();
	  glFlush();
	}

	inline float gLerp(float a, float b, float t)
	{
		return  a + t * (b - a);
	}

	inline float gDist(Geometry::Point pOne, Geometry::Point pTwo)
	{
		return sqrt(pow(pTwo.x - pOne.x, 2) + sqrt(pow(pTwo.y - pOne.y, 2)));
	}

	inline float bernstein(int k, int n, float u)
	{
		int j;
	    float coeff = 1.0;

	    for (j = n; j > k; j--)
	  	{
	  		coeff *= j;
	  	}         
	    for (j = n-k; j > k; j--)
	    {
	    	coeff /= j;
	    }

	    float bvl = coeff;

	    for (j = 1; j <= k; j++)
	    {
	    	bvl *= u;
	    }     
	    for (j = 1; j <= n-k; j++)
	    {
	    	bvl *= (1-u);
	    }     
	    return bvl;
	}

	inline void drawRectangle(int left, int right, int top, int bottom)
	{
		glColor3f(0.0, 1.0, 0.0); 
 		glBegin(GL_POLYGON); 
  		glVertex3f(left, bottom, 0.0); 
  		glVertex3f(left, top, 0.0); 
  		glVertex3f(right, top, 0.0); 
  		glVertex3f(right, bottom, 0.0);
 		glEnd(); 
 		glFlush(); 
	}
	
}