
#include "polygon.h"
#include "global.h"

myPoly::myPoly() : mControlPoints(), mBezierPoints(), mHasDerivatives(false) {}

myPoly::myPoly(vector<Geometry::Point> pCP) : mControlPoints(pCP), mBezierPoints(), mHasDerivatives(false) {}

void myPoly::addPoint(Geometry::Point pNewPoint)
{
	mControlPoints.push_back(pNewPoint);
}

void myPoly::addPoint(Geometry::Point pNewPoint, int pI)
{
  mControlPoints.insert( mControlPoints.begin() + pI, pNewPoint);
  drawBezier(0.001);
  drawControlPoly();
}

void myPoly::drawControlPoly()
{
	for (int i = 0; i < mControlPoints.size() - 1; i++)
	{
		global::drawLine(mControlPoints[i + 1], mControlPoints[i]);
	}

	if (mHasDerivatives)
	{
		assert(mDerivatives.size() == mControlPoints.size());
		for (int i = 0; i < mControlPoints.size(); i++)
		{
			global::drawLine(mControlPoints[i], mControlPoints[i] + mDerivatives[i]);
		}	
	}
}

void myPoly::deletePoint(int pI)
{
	mControlPoints.erase(mControlPoints.begin() + pI);
}

void myPoly::drawBezier(float pPrecision)
{
	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;

	vector<Geometry::Point> lIterationHelper;

	vector<Geometry::Point> lBezierPoints;

	//iterator
	double lT;

	// here is the double for loop! Only its a boule for loop with a while loop inside
	for (lT = 0; lT <= 1; lT += pPrecision)
	{
		// initally, all the points are present
		vector<Geometry::Point> lToBeReturned(mControlPoints.begin(), mControlPoints.end());

		while (lToBeReturned.size() > 1)
		{
			lIterationHelper.clear();

			// go over the size of the current vector
			for (int i = 0; i < lToBeReturned.size() - 1; i++)
			{
				lP1.x = lToBeReturned[i].x;
				lP1.y = lToBeReturned[i].y;

				lP2.x = lToBeReturned[i + 1].x;
				lP2.y = lToBeReturned[i + 1].y;

				lInterpolatedX = global::gLerp(lP1.x, lP2.x, lT);
				lInterpolatedY = global::gLerp(lP1.y, lP2.y, lT);

				lIterationHelper.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
			}

			lToBeReturned = lIterationHelper;
		}

		//the point has been computed, its the only element of lToBeReturned
		assert(lToBeReturned.size() == 1);
		lBezierPoints.push_back(lToBeReturned[0]);
	}

	if (lT != 1)
	{
		// push back the very last control point
		lBezierPoints.push_back(mControlPoints[mControlPoints.size() - 1]);
	}

	for(auto &cur : lBezierPoints)
	{
		global::drawDot(cur.x, cur.y);
	}

	cout << "End of Bezier computation" << endl;
}

void myPoly::drawBezierPoint()
{
	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;

	vector<Geometry::Point> lIterationHelper;
	vector<Geometry::Point> lBezierPoints;

	// initally, all the points are present
	vector<Geometry::Point> lToBeReturned(mControlPoints.begin(), mControlPoints.end());

	while (lToBeReturned.size() > 1)
	{
		lIterationHelper.clear();

		// go over the size of the current vector
		for (int i = 0; i < lToBeReturned.size() - 1; i++)
		{
			lP1.x = lToBeReturned[i].x;
			lP1.y = lToBeReturned[i].y;

			lP2.x = lToBeReturned[i + 1].x;
			lP2.y = lToBeReturned[i + 1].y;

			lInterpolatedX = global::gLerp(lP1.x, lP2.x, global::gT);
			lInterpolatedY = global::gLerp(lP1.y, lP2.y, global::gT);

			lIterationHelper.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
		}

		lToBeReturned = lIterationHelper;
	}
	// draw the point
	global::drawDot(lToBeReturned[0].x, lToBeReturned[0].y);
}

void myPoly::drawBezierBernstein(float pPrecision)
{
	float lT;
	for (lT = 0.0f; lT <= 1.0f; lT += pPrecision)
	{
		float x = 0.0;
		float y = 0.0;

		for (int i = 0; i < mControlPoints.size(); i++)
		{
			float b = global::bernstein(i, mControlPoints.size() - 1,lT);
			x += b * mControlPoints[i].x;
			y += b * mControlPoints[i].y;
		}
		global::drawDot(x,y);
	}
	cout << "End of Bezier-Bernstein computation" << endl;
}

void myPoly::raiseDegree()
{
	vector<Geometry::Point> lTemp = mControlPoints;
	int pDegree = lTemp.size() - 1;
	Geometry::Point lEndPoint = mControlPoints[mControlPoints.size() - 1];

	for (int i = 1; i < mControlPoints.size(); i++)
	{
		Geometry::Point lOne = lTemp[i - 1] * ((float)i / (float)(pDegree + 1));
		Geometry::Point lTwo = lTemp[i] * ((float)(pDegree + 1 - i) / (float)(pDegree + 1));
		mControlPoints[i] =  lOne + lTwo;
	}
	// add the last point -> raise degree
	mControlPoints.push_back(lEndPoint);

	// debugging you pussy
	assert(mControlPoints.size() == lTemp.size() + 1);
	assert(mControlPoints[0] == lTemp[0]);
	assert(mControlPoints[mControlPoints.size() - 1] == lTemp[lTemp.size() - 1]);
}

void myPoly::reduceDegree()
{
	vector<Geometry::Point> lLeft;
	vector<Geometry::Point> lRight;
	int pDegree = mControlPoints.size() - 1;

	// fill lLeft
	for (int i = 0; i < mControlPoints.size() - 1; i++)
	{
		Geometry::Point lOne = mControlPoints[i] * ((float)pDegree / (float)(pDegree - i));
		Geometry::Point lTwo(0.0f, 0.0f);
		if (i != 0)
		{
			lTwo = lLeft[i - 1] * ( -1.0f * ((float)i / (float)(pDegree - i)));
		}
		lLeft.push_back(lOne + lTwo);
	}
	assert(lLeft.size() == mControlPoints.size() - 1);

	// fill lRight
	for (int i = mControlPoints.size() - 1; i > 0; i--)
	{
		Geometry::Point lOne = mControlPoints[i] * ((float)pDegree / (float)i);
		Geometry::Point lTwo(0.0f, 0.0f);
		if (i != pDegree)
		{
			lTwo = lRight[lRight.size() - 1] * ( -1.0f * ((float)(pDegree - i) / (float)i));
		}
		lRight.push_back(lOne + lTwo);
	}
	assert(lRight.size() == mControlPoints.size() - 1);

	// average both lRight and lLeft
	mControlPoints.clear();
	for (int i = 0; i < lRight.size(); i++)
	{
		Geometry::Point lTempPoint = lRight[i];// + lLeft[lLeft.size() - 1 - i];
		mControlPoints.push_back(lTempPoint * 0.5);
	}
}

void myPoly::splitUpPoly(float pT)
{

	assert (mControlPoints.size() != 0);

	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;
	
	vector<Geometry::Point> lToBeReturned;
	vector<Geometry::Point> lCurPoints = mControlPoints;
	vector<Geometry::Point> lTemp;

	int lIterationCounter = 0;

	while (lCurPoints.size() > 1)
	{
		// add outside points into the final vector
		lToBeReturned.insert(lToBeReturned.begin() + lIterationCounter, lCurPoints[0]);
		lToBeReturned.insert(lToBeReturned.end() - lIterationCounter, lCurPoints[lCurPoints.size() - 1]);

		lTemp.clear();

		// go over the size of the current vector
		for (int i = 0; i < lCurPoints.size() - 1; i++)
		{
			lP1.x = lCurPoints[i].x;
			lP1.y = lCurPoints[i].y;

			lP2.x = lCurPoints[i + 1].x;
			lP2.y = lCurPoints[i + 1].y;

			lInterpolatedX = global::gLerp(lP1.x, lP2.x, pT);
			lInterpolatedY = global::gLerp(lP1.y, lP2.y, pT);

			lTemp.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
		}
		lCurPoints = lTemp;
		lIterationCounter++;
	}

	// add the very last middle point
	assert(lCurPoints.size() == 1);
	lToBeReturned.insert(lToBeReturned.begin() + lIterationCounter, lCurPoints[0]);

	

	assert(lToBeReturned.size() % 2 != 0);
	int middle = (lToBeReturned.size() + 1) / 2;

	std::vector<Geometry::Point> newControlPoints(lToBeReturned.begin(), lToBeReturned.begin() + middle);

	// memory deallocation (as good as it gets....)
	lCurPoints.clear();
	lTemp.clear();

	mControlPoints.clear();
	for (int i = 0; i < newControlPoints.size(); i++)
	{
		mControlPoints.push_back(newControlPoints.at(i));
	}
	std::vector<Geometry::Point> addMe(lToBeReturned.begin() + middle - 1, lToBeReturned.end());

	mSplitUp.clear();
	mSplitUp.push_back(mControlPoints);
	mSplitUp.push_back(addMe);
	if (mSplitUp.size() != 2)
	{
		int a = 0;
	}
	assert(mSplitUp.size() == 2);

	cout << "Splitting computation has ended" << endl;	
}

void myPoly::drawAitkenPolygon(float pPrecision)
{
	for (float i = pPrecision; i < 1.0f; i += pPrecision)
	{
		drawAitkenJustPoint(i);
	}
}

void myPoly::drawAitkenJustPoint(float pPrecision)
{
	vector<float> pDeltas;
	vector<float> pTs;
	float pTotalDelta = 0.0f;

	vector<Geometry::Point> lTempPoints = mControlPoints;

	for (int i = 1; i < mControlPoints.size(); i++)
	{
		float temp = abs(global::gDist(mControlPoints[i - 1], mControlPoints[i]));
		pDeltas.push_back(temp);
		pTotalDelta += temp;
	}

	// normalize the deltas into a range from 0-1
	for (auto& cur : pDeltas)
	{
		cur /= pTotalDelta;
	}
	
	// set up the t values:
	float sum = 0.0f;
	pTs.push_back(sum);
	for (int i = 0; i < pDeltas.size(); i++)
	{
		sum += pDeltas[i];
		pTs.push_back(sum);
	}
	assert(pDeltas.size() == pTs.size() - 1);

	int iteration = 0;
	while (lTempPoints.size() > 1)
	{
		for (int i = 0; i < lTempPoints.size() - 1; i++)
		{
			// logic for computing the bottom of the fraction
			float bottom = pDeltas[i];
			for (int it = 0; it < iteration; it++)
			{
				bottom += pDeltas[i + 1 + it];
			}

			Geometry::Point lOne = lTempPoints[i] * ((pTs[i + 1 + iteration] - (float)pPrecision) / bottom);
			Geometry::Point lTwo = lTempPoints[i + 1] * (((float)pPrecision - pTs[i]) / bottom);
			lTempPoints[i] =  lOne + lTwo; 
		}

		// delete last point of this iteration
		auto it = lTempPoints.end();
		lTempPoints.erase(it);

		// increment iteration
		iteration++;
	}

	assert(lTempPoints.size() == 1);
	global::drawDot(lTempPoints[0].x, lTempPoints[0].y);
}

void myPoly::drawAitkenPoint(float pPrecision)
{
	vector<float> pDeltas;
	vector<float> pTs;
	float pTotalDelta = 0.0f;

	vector<Geometry::Point> lTempPoints = mControlPoints;

	for (int i = 1; i < mControlPoints.size(); i++)
	{
		float temp = abs(global::gDist(mControlPoints[i - 1], mControlPoints[i]));
		pDeltas.push_back(temp);
		pTotalDelta += temp;
	}

	// normalize the deltas into a range from 0-1
	for (auto& cur : pDeltas)
	{
		cur /= pTotalDelta;
	}
	
	// set up the t values:
	float sum = 0.0f;
	pTs.push_back(sum);
	for (int i = 0; i < pDeltas.size(); i++)
	{
		sum += pDeltas[i];
		pTs.push_back(sum);
	}
	assert(pDeltas.size() == pTs.size() - 1);

	int iteration = 0;
	while (lTempPoints.size() > 1)
	{
		for (int i = 0; i < lTempPoints.size() - 1; i++)
		{
			// logic for computing the bottom of the fraction
			float bottom = pDeltas[i];
			for (int it = 0; it < iteration; it++)
			{
				bottom += pDeltas[i + 1 + it];
			}

			Geometry::Point lOne = lTempPoints[i] * ((pTs[i + 1 + iteration] - (float)pPrecision) / bottom);
			Geometry::Point lTwo = lTempPoints[i + 1] * (((float)pPrecision - pTs[i]) / bottom);
			lTempPoints[i] =  lOne + lTwo; 
		}

		// delete last point of this iteration
		auto it = lTempPoints.end();
		lTempPoints.erase(it);

		// drawing intermediate control poly
		if (iteration == 0)
		{
			global::drawLine(mControlPoints[0], lTempPoints[0]);	
		}
		for (int i = 0; i < lTempPoints.size() - 1; i++)
		{
			global::drawLine(lTempPoints[i + 1], lTempPoints[i]);
		}
		if (iteration == 0)
		{
			global::drawLine(lTempPoints[lTempPoints.size() - 1], mControlPoints[mControlPoints.size() - 1]);
		}

		// increment iteration
		iteration++;
	}

	assert(lTempPoints.size() == 1);
	global::drawDot(lTempPoints[0].x, lTempPoints[0].y);
}

void myPoly::derivatives()
{
	assert(mControlPoints.size() % 2 == 0);

	mHasDerivatives = true;
	int lInitialSize = mControlPoints.size();
	mDerivatives.clear();
	for (int i = 1; i < lInitialSize; i += 2)
	{
		mDerivatives.push_back( mControlPoints[i] + (mControlPoints[i - 1] * -1) );
	}
	for (int  i = mControlPoints.size() - 1; i > 0; i -= 2)
	{
		mControlPoints.erase(mControlPoints.begin() + i);
	}
}

void myPoly::spline()
{
	if (mHasDerivatives)
	{
		for (int i = 0; i < mControlPoints.size() - 1; i++)
		{
			vector<Geometry::Point> lControlPoly;
			lControlPoly.push_back(mControlPoints[i]);
			lControlPoly.push_back(mControlPoints[i] + (mDerivatives[i] * (1.0 / 3.0)) );
			lControlPoly.push_back(mControlPoints[i + 1] + (mDerivatives[i + 1] * (-1.0 / 3.0)) );
			lControlPoly.push_back(mControlPoints[i + 1]);

			myPoly lDrawMe(lControlPoly);
			lDrawMe.drawBezier(0.001);
		}
	}
	else
	{
		mDerivatives.clear();
		mDerivatives.push_back(mControlPoints[1] + (mControlPoints[0] * (-1)));
		for (int i = 1; i < mControlPoints.size() - 1; i++)
		{
			mDerivatives.push_back(mControlPoints[i + 1] + (mControlPoints[i - 1] * (-1)));
		}
		mDerivatives.push_back(mControlPoints[mControlPoints.size() - 1] + (mControlPoints[mControlPoints.size() - 2] * (-1)));

		assert(mDerivatives.size() == mControlPoints.size());

		mHasDerivatives = true;
		spline();
	}
}