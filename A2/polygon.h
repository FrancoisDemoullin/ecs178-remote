#pragma once

#include <iostream>
#include <vector>

#include "geometry.h"

using namespace std;

class myPoly
{

public:
	myPoly();
	myPoly(vector<Geometry::Point> pCP);

	void addPoint(Geometry::Point pNewPoint);
	void addPoint(Geometry::Point pNewPoint, int pI);
	void deletePoint(int pI);

	void drawBezier(float pPrecision);
	void drawBezierBernstein(float pPrecision);
	void drawBezierPoint();
	void drawControlPoly();

	void drawAitkenPoint(float pT);
	void drawAitkenPolygon(float pT);

	void raiseDegree();
	void reduceDegree();

	void splitUpPoly(float pT);

	void derivatives();
	void spline();

	vector<vector<Geometry::Point>> mSplitUp;
	vector<Geometry::Point> mControlPoints;

private:
	vector<Geometry::Point> mBezierPoints;
	vector<Geometry::Point> mAitkenPoints;

	vector<Geometry::Point> mDerivatives;

	bool mHasDerivatives;

	void drawAitkenJustPoint(float pPrecision);
};
