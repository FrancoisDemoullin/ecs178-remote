
#include "intersection.h"
#include "global.h"

void Intersection::computeIntersection(myPoly& pA, myPoly& pB)
{
	float lAOneLeft, lAOneRight, lAOneTop, lAOneBottom;
	float lATwoLeft, lATwoRight, lATwoTop, lATwoBottom;

	float lBOneLeft, lBOneRight, lBOneTop, lBOneBottom;
	float lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom;

	// split both polygons, t  = 0.5
	pA.splitUpPoly(0.5); assert(pA.mSplitUp.size() == 2);
	pB.splitUpPoly(0.5); assert(pB.mSplitUp.size() == 2);

	// get the bounding boxes
	MBBHelper(pA.mSplitUp[0], lAOneLeft, lAOneRight, lAOneTop, lAOneBottom);
	MBBHelper(pA.mSplitUp[1], lATwoLeft, lATwoRight, lATwoTop, lATwoBottom);
	MBBHelper(pB.mSplitUp[0], lBOneLeft, lBOneRight, lBOneTop, lBOneBottom);
	MBBHelper(pB.mSplitUp[1], lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom);
	
	// compare bounding boxes
	myPoly lAOne(pA.mSplitUp[0]);
	myPoly lATwo(pA.mSplitUp[1]);
	myPoly lBOne(pB.mSplitUp[0]);
	myPoly lBTwo(pB.mSplitUp[1]);

	if (intersection(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom, lBOneLeft, lBOneRight, lBOneTop, lBOneBottom))
	{
		if (computeArea(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom);
		}
		else if (computeArea(lBOneLeft, lBOneRight, lBOneTop, lBOneBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lBOneLeft, lBOneRight, lBOneTop, lBOneBottom);
		}
		else
		{
			// recurse
			computeIntersection(lAOne, lBOne);
		}
	}
	if (intersection(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom, lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom))
	{
		if (computeArea(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lAOneLeft, lAOneRight, lAOneTop, lAOneBottom);
		}
		else if (computeArea(lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom);
		}
		else
		{
			// recurse
			computeIntersection(lAOne, lBTwo);
		}	
	}
	if (intersection(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom, lBOneLeft, lBOneRight, lBOneTop, lBOneBottom))
	{
		if (computeArea(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom);
		}
		else if (computeArea(lBOneLeft, lBOneRight, lBOneTop, lBOneBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lBOneLeft, lBOneRight, lBOneTop, lBOneBottom);
		}
		else
		{
			// recurse
			computeIntersection(lATwo, lBOne);
		}	
	}
	if (intersection(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom, lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom))
	{
		if (computeArea(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lATwoLeft, lATwoRight, lATwoTop, lATwoBottom);
		}
		else if (computeArea(lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom) <= global::gAreaCutoff)
		{
			cout << "Intersection was found" << endl;
			global::drawRectangle(lBTwoLeft, lBTwoRight, lBTwoTop, lBTwoBottom);
		}
		else
		{
			// recurse
			computeIntersection(lATwo, lBTwo);
		}	
	} 
}

void Intersection::MBBHelper(vector<Geometry::Point>& points, float& pLeft, float& pRight, float& pTop, float& pBottom)
{
	pLeft = 1000000;
	pRight = -1;
	pTop = -1;
	pBottom = 10000000;

	for (auto &cur: points)
	{
		if (cur.y > pTop)
		{
			pTop = cur.y;
		}
		if (cur.y < pBottom)
		{
			pBottom = cur.y;
		}
		if (cur.x > pRight)
		{
			pRight = cur.x;
		}
		if (cur.x < pLeft)
		{
			pLeft = cur.x;
		}
	}

	assert(pLeft < pRight);
	assert(pTop > pBottom);
}

bool Intersection::intersection(float pALeft, float pARight, float pATop, float pABottom, float pBLeft, float pBRight, float pBTop, float pBBottom)
{
	return (pALeft < pBRight && pARight > pBLeft && pATop > pBBottom && pABottom < pBTop);
}

float Intersection::computeArea(float pALeft, float pARight, float pATop, float pABottom)
{
	float area = (pARight - pALeft) * (pATop - pABottom);
	assert(area > 0.0);
	return area;
}