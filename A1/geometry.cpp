#include "geometry.h"

using namespace Geometry;

bool Segment::isOnLine(float pX, float pY)
{
	// (b - a) cross (c - a) => checks for alignment of the 3 points
	float lCrossProduct = (pY - mP0.y) * (mP1.x - mP0.x) - (pX - mP0.x) * (mP1.y - mP0.y);

	if (fabs(lCrossProduct) > INTERSECTION_EPSILON)
	{
		return false;
	}

	// (b - a) dot (c - a), if negative no projection => not in bewteen
	auto lDotProduct = (pX - mP0.x) * (mP1.x - mP0.x) + (pY - mP0.y) * (mP1.y - mP0.y);
	if (lDotProduct < 0)
	{
		return false;
	}

	// dot product was positive, length needs to be less than sqared distance between the 2 end points
	auto lSquaredLength = (mP1.x - mP0.x) * (mP1.x - mP0.x) + (mP1.y - mP0.y) * (mP1.y - mP0.y);
	if (lDotProduct > lSquaredLength)
	{
		return false;
	}

	// all test passed, return true
	return true;
}

float Segment::getSlope()
{
	if ((mP1.x - mP0.x) == 0)
	{
		return INFINITY;
	}
	// rise over run. Overly casted! I am sorry, just making sure...
	return (float)((float)(mP1.y - mP0.y) / (float)(mP1.x - mP0.x));
}






/*
Point Line::computeIntersection(Line pOther)
{
	float lX;
	float lY;

	if (pOther.isInf)
	{
		lX = pOther.mP0.x;
		lY = mSlope * lX + mB;
	}
	else if (isInf)
	{
		lX = mP0.x;
		lY = pOther.mSlope * lX + pOther.mB;
	}
	else
	{
		lX = (mB - pOther.mB) / (pOther.mSlope - mSlope);
		lY = mSlope * lX + mB;
	}

	return Point(lX, lY);
}

vector<Point> Line::computeIntersections(vector<Segment>& pSeg, vector<int>& pEdgeIndexVector)
{
	vector<Point> lToBeReturned;

	// computes the area of the sub-area
	int indexTracker = 0;

	for (auto& currSeg : pSeg)
	{
		float lSlope = currSeg.getSlope();
		bool degenerate = false;
		if (lSlope == INFINITY)
		{
			degenerate = true;
		}
		Line lOther(currSeg.mP1, lSlope, degenerate);

		Point lCandidate;
		if (mSlope != lOther.mSlope)
		{
			Point lTemp = computeIntersection(lOther);
			lCandidate.setX(lTemp.x);
			lCandidate.setY(lTemp.y);
		}
		else
		{
			lCandidate.setX(mP0.x);
			lCandidate.setY(mP0.y);
		}
		if (currSeg.isOnLine(lCandidate.x, lCandidate.y) && lCandidate.x >= 0 && lCandidate.y >= 0)
		{
			// we found a valid intersection
			lToBeReturned.push_back(lCandidate);
			pEdgeIndexVector.push_back(indexTracker);
		}
		else
		{
			int a = 0;
		}
		indexTracker++;
	}

	return lToBeReturned;
}

vector<Point> Line::computeBoundaryIntersections()
{
	vector<Point> lToBeReturned;

	// computes the area of the sub-area
	int indexTracker = 0;

	for (auto& currSeg : global::gBoundary)
	{
		float lSlope = currSeg.getSlope();
		bool degenerate = false;
		if (lSlope == INFINITY)
		{
			degenerate = true;
		}
		Line lOther(currSeg.mP1, lSlope, degenerate);

		Point lCandidate;
		if (mSlope != lOther.mSlope)
		{
			Point lTemp = computeIntersection(lOther);
			lCandidate.setX(lTemp.x);
			lCandidate.setY(lTemp.y);
		}
		else
		{
			lCandidate.setX(mP0.x);
			lCandidate.setY(mP0.y);
		}
		switch (indexTracker) 
		{
		case 0:
			if (-0.01 < lCandidate.x && 0.01 > lCandidate.x)
			{
				if (0.0 <= lCandidate.y && 600.0 >= lCandidate.y)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 1:
			if (-0.01 < lCandidate.y && 0.01 > lCandidate.y)
			{
				if (0.0 <= lCandidate.x && 600.0 >= lCandidate.x)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 2:
			if (599.99 < lCandidate.y && 600.01 > lCandidate.y)
			{
				if (0.0 <= lCandidate.x && 600.0 >= lCandidate.x)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		case 3:
			if (599.99 < lCandidate.x && 600.01 > lCandidate.x)
			{
				if (0.0 <= lCandidate.y && 600.0 >= lCandidate.y)
				{
					lToBeReturned.push_back(lCandidate);
				}
			}
			break;
		default:
			assert(1 == 0);
		}
		indexTracker++;
	}

	return lToBeReturned;
}
*/

