
#include "geometry.h"
#include "polygon.h"

namespace Intersection
{
	void computeIntersection(myPoly& pA, myPoly& pB);
	void MBBHelper(vector<Geometry::Point>& points, float& pLeft, float& pRight, float& pTop, float& pBottom);
	bool intersection(float pALeft, float pARight, float pATop, float pABottom, float pBLeft, float pBRight, float pBTop, float pBBottom);
	float computeArea(float pALeft, float pARight, float pATop, float pABottom);
}