
#include "polygon.h"
#include "global.h"

myPoly::myPoly() : mControlPoints(), mBezierPoints() {}

myPoly::myPoly(vector<Geometry::Point> pCP) : mControlPoints(pCP), mBezierPoints() {}

void myPoly::addPoint(Geometry::Point pNewPoint)
{
	mControlPoints.push_back(pNewPoint);
}

void myPoly::addPoint(Geometry::Point pNewPoint, int pI)
{
  mControlPoints.insert( mControlPoints.begin() + pI, pNewPoint);
  drawBezier(0.001);
  drawControlPoly();
}

void myPoly::drawControlPoly()
{
	for (int i = 0; i < mControlPoints.size() - 1; i++)
	{
		global::drawLine(mControlPoints[i + 1], mControlPoints[i]);
	}
}

void myPoly::deletePoint(int pI)
{
	mControlPoints.erase(mControlPoints.begin() + pI);
}

void myPoly::drawBezier(float pPrecision)
{
	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;

	vector<Geometry::Point> lIterationHelper;

	vector<Geometry::Point> lBezierPoints;

	//iterator
	double lT;

	// here is the double for loop! Only its a boule for loop with a while loop inside
	for (lT = 0; lT <= 1; lT += pPrecision)
	{
		// initally, all the points are present
		vector<Geometry::Point> lToBeReturned(mControlPoints.begin(), mControlPoints.end());

		while (lToBeReturned.size() > 1)
		{
			lIterationHelper.clear();

			// go over the size of the current vector
			for (int i = 0; i < lToBeReturned.size() - 1; i++)
			{
				lP1.x = lToBeReturned[i].x;
				lP1.y = lToBeReturned[i].y;

				lP2.x = lToBeReturned[i + 1].x;
				lP2.y = lToBeReturned[i + 1].y;

				lInterpolatedX = global::gLerp(lP1.x, lP2.x, lT);
				lInterpolatedY = global::gLerp(lP1.y, lP2.y, lT);

				lIterationHelper.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
			}

			lToBeReturned = lIterationHelper;
		}

		//the point has been computed, its the only element of lToBeReturned
		assert(lToBeReturned.size() == 1);
		lBezierPoints.push_back(lToBeReturned[0]);
	}

	if (lT != 1)
	{
		// push back the very last control point
		lBezierPoints.push_back(mControlPoints[mControlPoints.size() - 1]);
	}

	for(auto &cur : lBezierPoints)
	{
		global::drawDot(cur.x, cur.y);
	}

	cout << "End of Bezier computation" << endl;
}

void myPoly::drawBezierPoint()
{
	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;

	vector<Geometry::Point> lIterationHelper;
	vector<Geometry::Point> lBezierPoints;

	// initally, all the points are present
	vector<Geometry::Point> lToBeReturned(mControlPoints.begin(), mControlPoints.end());

	while (lToBeReturned.size() > 1)
	{
		lIterationHelper.clear();

		// go over the size of the current vector
		for (int i = 0; i < lToBeReturned.size() - 1; i++)
		{
			lP1.x = lToBeReturned[i].x;
			lP1.y = lToBeReturned[i].y;

			lP2.x = lToBeReturned[i + 1].x;
			lP2.y = lToBeReturned[i + 1].y;

			lInterpolatedX = global::gLerp(lP1.x, lP2.x, global::gT);
			lInterpolatedY = global::gLerp(lP1.y, lP2.y, global::gT);

			lIterationHelper.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
		}

		lToBeReturned = lIterationHelper;
	}
	// draw the point
	global::drawDot(lToBeReturned[0].x, lToBeReturned[0].y);
}

/*TODO:
	add support for 3, then add supporrt for 5???
	Piazza pontentially
*/
void myPoly::drawBezierBernstein(float pPrecision)
{
	float lT;
	for (lT = 0.0f; lT <= 1.0f; lT += pPrecision)
	{
		float x = 0.0;
		float y = 0.0;

		for (int i = 0; i < mControlPoints.size(); i++)
		{
			float b = global::bernstein(i, mControlPoints.size() - 1,lT);
			x += b * mControlPoints[i].x;
			y += b * mControlPoints[i].y;
		}
		global::drawDot(x,y);
	}
	cout << "End of Bezier-Bernstein computation" << endl;
}

void myPoly::splitUpPoly(float pT)
{

	assert (mControlPoints.size() != 0);

	// variable definitons
	Geometry::Point lP1(0.0f, 0.0f);
	Geometry::Point lP2(0.0f, 0.0f);

	float lInterpolatedX;
	float lInterpolatedY;
	
	vector<Geometry::Point> lToBeReturned;
	vector<Geometry::Point> lCurPoints = mControlPoints;
	vector<Geometry::Point> lTemp;

	int lIterationCounter = 0;

	while (lCurPoints.size() > 1)
	{
		// add outside points into the final vector
		lToBeReturned.insert(lToBeReturned.begin() + lIterationCounter, lCurPoints[0]);
		lToBeReturned.insert(lToBeReturned.end() - lIterationCounter, lCurPoints[lCurPoints.size() - 1]);

		lTemp.clear();

		// go over the size of the current vector
		for (int i = 0; i < lCurPoints.size() - 1; i++)
		{
			lP1.x = lCurPoints[i].x;
			lP1.y = lCurPoints[i].y;

			lP2.x = lCurPoints[i + 1].x;
			lP2.y = lCurPoints[i + 1].y;

			lInterpolatedX = global::gLerp(lP1.x, lP2.x, pT);
			lInterpolatedY = global::gLerp(lP1.y, lP2.y, pT);

			lTemp.push_back(Geometry::Point(lInterpolatedX, lInterpolatedY));
		}
		lCurPoints = lTemp;
		lIterationCounter++;
	}

	// add the very last middle point
	assert(lCurPoints.size() == 1);
	lToBeReturned.insert(lToBeReturned.begin() + lIterationCounter, lCurPoints[0]);

	cout << "Splitting computation has ended" << endl;

	assert(lToBeReturned.size() % 2 != 0);
	int middle = (lToBeReturned.size() + 1) / 2;

	std::vector<Geometry::Point> newControlPoints(lToBeReturned.begin(), lToBeReturned.begin() + middle);

	// memory deallocation (as good as it gets....)
	lCurPoints.clear();
	lTemp.clear();

	mControlPoints.clear();
	for (int i = 0; i < newControlPoints.size(); i++)
	{
		mControlPoints.push_back(newControlPoints.at(i));
	}
	std::vector<Geometry::Point> addMe(lToBeReturned.begin() + middle - 1, lToBeReturned.end());

	mSplitUp.clear();
	mSplitUp.push_back(mControlPoints);
	mSplitUp.push_back(addMe);
}