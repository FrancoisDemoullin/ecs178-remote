
#include "Line.h"

using namespace std;

myLine::myLine(int pA, int pB, int pC, int pD, char* pMode, float pRed, float pGreen, float pBlue)
{
	

	if (*pMode == 'B')
	{
		myLine::drawLineBresenham(pA, pB, pC, pD, pRed, pGreen, pBlue);
	}
	else if (*pMode == 'D')
	{
		myLine::drawLineDDA(pA, pB, pC, pD, pRed, pGreen, pBlue);
	}
	else
	{
		int a = 1;
	}

	a = pA;
	b = pB;
	c = pC;
	d = pD;
}

/*
Tested under the following circumstances
	positive slope (steep and shallow)
	negative slope (steep and shallow)
	vertical line
	horizontal line
	step verticlly
	step horizontally
all tests passed
*/
void myLine::drawLineBresenham(int pA, int pB, int pC, int pD, float pRed, float pGreen, float pBlue)
{
	// swap helper
	int lTemp;

	// check if we should step through vertically or horizontally
	bool steep = (fabs(pD - pB) > fabs(pC - pA));

	// if slope is bigger than one, swap witin couples
	if (steep)
	{
		// swap first couple
		lTemp = pA;
		pA = pB;
		pB = lTemp;

		// swap second couple
		lTemp = pC;
		pC = pD;
		pD = lTemp;
	}

	// if the slope is negative (we know that abs(slope) < 1 now)
	if (pA > pC)
	{
		// swap Xs accross couples
		lTemp = pA;
		pA = pC;
		pC = lTemp;

		// swap Ys accross couples
		lTemp = pB;
		pB = pD;
		pD = lTemp;
	}

	// calculate rise and run
	float lDeltaX = pC - pA;
	float lDeltaY = fabs(pD - pB);

	// formula
	float error = lDeltaX / 2.0f;
	int ystep = (pB < pD) ? 1 : -1;
	int y = pB;

	int maxX = pC;

	for (int x = pA; x < maxX; x++)
	{
		if (steep)
		{
			gMakePixelColor(y, x, pRed, pBlue, pGreen);
		}
		else
		{
			gMakePixelColor(x, y, pRed, pBlue, pGreen);
		}

		error -= lDeltaY;
		if (error < 0)
		{
			y += ystep;
			error += lDeltaX;
		}
	}
}

/*
Tested under the following circumstances
	positive slope (steep and shallow)
	negative slope (steep and shallow)
	vertical line
	horizontal line
	step verticlly
	step horizontally
all tests passed
*/
void myLine::drawLineDDA(int pA, int pB, int pC, int pD, float pRed, float pGreen, float pBlue)
{
	// change in x and y
	float lDeltaX, lDeltaY;
	// number of steps there are between the two points
	int lSteps;
	// increment values
	float lXinc, lYinc;
	//count
	int lCount;

	// caluclate rise and run
	lDeltaX = pA - pC;
	lDeltaY = pB - pD;

	// decide in which direction we should step: either step vertcally or horizontally
	if (fabs(lDeltaX) > fabs(lDeltaY))
	{
		// step horizontally if change in x is bigger than change in y
		lSteps = fabs(lDeltaX);
	}
	else
	{
		// step vertically if change in y is bigger than change in x
		lSteps = fabs(lDeltaY);
	}

	// compute the increments for each direction
	lXinc = lDeltaX / lSteps;
	lYinc = lDeltaY / lSteps;

	// change the input params to floats, so we can decrement them properly
	float lA = (float)pA;
	float lB = (float)pB;

	for (lCount = 0; lCount < lSteps; lCount++)
	{
		// draw pixel on line
		gMakePixelColor(floor(lA), floor(lB), pRed, pGreen, pBlue);
		// update helpers by "stepping once"
		lA -= lXinc;
		lB -= lYinc;
	}
	
}