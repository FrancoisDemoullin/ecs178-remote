#pragma once

// lib includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

// my includes
#include "Utility.h"
#include "Line.h"
#include "Polygon.h"


using namespace std;

class FileHandler {
public:
	// open File is done in Constructor
	FileHandler(const char* pFilepath);
	
	// interpret the file and exectute the corresponding instruction
	void interpretFile(vector<myPolygon>& pP, vector<myLine>& pL);

	inline vector<string> getFileLines()
	{
		return mFileLines;
	};
	inline void setFileLines(vector<string>& pFileLines)
	{
		mFileLines = pFileLines;
	};
private:
	vector<string> mFileLines;

	void interpretLine(std::string pLineString, vector<myPolygon>& pP, vector<myLine>& pL);
	vector<int> findIntsInLine(string pLineString);
};
