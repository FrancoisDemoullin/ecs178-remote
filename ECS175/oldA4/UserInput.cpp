#include "UserInput.h"
#include <assert.h>


using namespace std;

void UserInput::displaySetupInput()
{
	// display some initial bla bla bla bla
	//cout << "Please enter the screen width and the screen height you desire" << endl;
	//cout << "Please be reasonable in your choice. Your input should just be two numbers, width and height separated by a space." << endl;

	// get user's input
	//cin >> gScreenHeight >> gScreenWidth;

	// hardcoding for Debugging
	gScreenHeight = 800;
	gScreenWidth = 800;

	// prompting the user for k and t
	cout << "Please enter the precision that will be applied to all curves (in clas we called it t)" << endl;
	cin >> gPrecision;
	cout << "please enter the k that will be applied to all Bspline curves (make sure k is smaller than the smalles Bspline control polygon in the scene)" << endl;
	cin >> gK;

	// pointless text
	cout << "Thank you, your window is being created." << endl;
}

void UserInput::displayInteractionInput(vector<myPolygon>& gDrawnPolys, vector<myLine>& gLines)
{
	
	gPrintSeperatorToConsole();

	string userString;
	cin.clear();
	cout << "INTERACTION UI!!! Your input is required! Here are your options:" << endl;
	cout << "1. You can add a control point to a polygon: EX: 'add (1) (1) (400, 400)'" << endl;
	cout << "2. You can delete a control point from a polygon: EX: 'remove (2) (2)'" << endl;
	cout << "3. You can change the corrdinates of a contol point: EX: change (2) (2) (400, 400)" << endl;
	cout << "4. If you want more information about the polygons type: 'info'" << endl;
	cout << "5. Change the precision for all the polygons by typing: precision (0.0005)" << endl;
	cout << "6. Change the k for all the Bspline curves by typing: k (4)" << endl;

	getline(cin, userString);
	// get user input
	if (!userString.empty())
	{
		interpretUserInput(userString, gDrawnPolys, gLines);
	}
}

void UserInput::interpretUserInput(string pInputString, vector<myPolygon>& gDrawnPolys, vector<myLine>& gLines)
{
	if (pInputString.find("info") != std::string::npos)
	{
		// the user wants more information about the polygons
		gPrintSeperatorToConsole();

		cout << "There are currently " << gDrawnPolys.size() << " drawn on the screen." << endl;

		int lIndexHelper;

		for (int i = 0; i < gDrawnPolys.size(); i++)
		{
			// just making sure some basic rules of the polygons havent been violated
			assert((gDrawnPolys[i].getVector().size() - 1) % 2 == 0);

			lIndexHelper = 1;

			cout << "This is information about Polyon Number (" << i + 1 << ")" << endl;
			cout << "This polygon has (" << (gDrawnPolys[i].getVector().size() - 1) / 2<< ") control points" << endl;
			for (int j = 1; j < gDrawnPolys[i].getVector().size(); j+=2)
			{
				cout << "Control point (" << lIndexHelper << ") is at (" << gDrawnPolys[i].getVector()[j] << ", " << gDrawnPolys[i].getVector()[j + 1] << ")" << endl;
				lIndexHelper++;
			}
		}

	}
	else if (pInputString.find("add") != std::string::npos)
	{
		vector<int> lIntsInInput = UserInput::findIntsInLineUser(pInputString);

		int lPolyId = lIntsInInput[0] - 1;
		int lIndexToBeInserted = lIntsInInput[1];
		

		// run some sanity checks on the user input
		assert(0 <= lPolyId && lPolyId < gDrawnPolys.size());
		assert(lIntsInInput.size() == 4);

		myPolygon lPolytoBeManipulated = gDrawnPolys[lPolyId];
		vector<int> lOldVector = lPolytoBeManipulated.getVector();
		vector<int> lNewVector = lOldVector;

		// run some more sanity checks
		assert(0 <= lIndexToBeInserted && lIndexToBeInserted <= lPolytoBeManipulated.getVector().size());

		// insert the new element into the vector
		lNewVector.insert(lNewVector.begin() + 1 + lIndexToBeInserted * 2, lIntsInInput[2]);
		lNewVector.insert(lNewVector.begin() + 1 + lIndexToBeInserted * 2 + 1, lIntsInInput[3]);

		// increment the number of points in the vector
		lNewVector[0]++;

		// update gloabl poly structure
		gDrawnPolys[lPolyId].setVector(lNewVector);

		// redraw entire scene after updating
		redrawScene(gDrawnPolys);
	}
	else if (pInputString.find("remove") != std::string::npos)
	{
		vector<int> lIntsInInput = UserInput::findIntsInLineUser(pInputString);

		int lPolyId = lIntsInInput[0] - 1;
		int lIndexToBeDeleted = lIntsInInput[1];


		// run some sanity checks on the user input
		assert(0 <= lPolyId && lPolyId < gDrawnPolys.size());
		assert(lIntsInInput.size() == 2);

		myPolygon lPolytoBeManipulated = gDrawnPolys[lPolyId];
		vector<int> lOldVector = lPolytoBeManipulated.getVector();
		vector<int> lNewVector = lOldVector;

		// run some more sanity checks
		assert(0 <= lIndexToBeDeleted && lIndexToBeDeleted < lPolytoBeManipulated.getVector().size() - 1);

		// delete the point at the index lToBeDeleted
		lNewVector.erase(lNewVector.begin() + 1 + lIndexToBeDeleted * 2);
		lNewVector.erase(lNewVector.begin() + 1 + lIndexToBeDeleted * 2);

		// decrement the number of points in the vector
		lNewVector[0]--;

		// update gloabl poly structure
		gDrawnPolys[lPolyId].setVector(lNewVector);

		// redraw entire scene after updating
		redrawScene(gDrawnPolys);
	}
	else if (pInputString.find("change") != std::string::npos)
	{
		vector<int> lIntsInInput = UserInput::findIntsInLineUser(pInputString);

		int lPolyId = lIntsInInput[0] - 1;
		int lIndexToBeChanged = lIntsInInput[1];


		// run some sanity checks on the user input
		assert(0 <= lPolyId && lPolyId < gDrawnPolys.size());
		assert(lIntsInInput.size() == 4);

		myPolygon lPolytoBeManipulated = gDrawnPolys[lPolyId];
		vector<int> lOldVector = lPolytoBeManipulated.getVector();

		// run some more sanity checks
		assert(0 <= lIndexToBeChanged && lIndexToBeChanged < lPolytoBeManipulated.getVector().size() - 1);

		// update the point to the new vector
		lOldVector[1 + lIndexToBeChanged * 2] = lIntsInInput[2];
		lOldVector[1 + lIndexToBeChanged * 2 + 1] = lIntsInInput[3];

		// update gloabl poly structure
		gDrawnPolys[lPolyId].setVector(lOldVector);

		// redraw entire scene after updating
		redrawScene(gDrawnPolys);
	}
	else if (pInputString.find("save") != std::string::npos)
	{
		ofstream myfile("output.txt");
		if (myfile.is_open())
		{
			for (int i = 0; i < gDrawnPolys.size(); i++)
			{
				if (gDrawnPolys[i].getIsBezier())
				{
					myfile << "Polygon Bezier (" << gDrawnPolys[i].getVector()[0] << ") ";
				}
				else
				{
					myfile << "Polygon Bspline (" << gDrawnPolys[i].getVector()[0] << ") ";
				}
				for (int j = 1; j < gDrawnPolys[i].getVector().size(); j += 2)
				{
					myfile << "(" << gDrawnPolys[i].getVector()[j] << ", " << gDrawnPolys[i].getVector()[j + 1] << ") ";
				}
				myfile << "\n";
			}
			myfile.close();
		}
	}
	else if (pInputString.find("precision") != std::string::npos)
	{
		int a = findFloatsInLineUser(pInputString).size();
		if (findFloatsInLineUser(pInputString).size() == 1)
		{
			gPrecision = findFloatsInLineUser(pInputString)[0];
		}
	}
	else
	{
		// user input was not to be understood
		cout << "I did not understand your input! Please enter one of the options available to you. Make sure there are no typos, the dumb machine does not like typos...." << endl;
	}
}

vector<int> UserInput::findIntsInLineUser(string pLineString)
{
	// transform string into local stringstream
	stringstream lStream(pLineString);

	// vector to be returned
	vector<int> lToBeReturned;

	// parse string to get 4 ints following the following convention:
	// (INT1, INT2)(INT3, INT4)
	size_t lFirstBracketIndex = pLineString.find('(');
	pLineString = pLineString.erase(0, lFirstBracketIndex + 1); // it now is in the form we want it to be: INT1, INT2)(INT3, INT4)

	std::stringstream lSs(pLineString);

	int i;
	bool lIsNextNumberNegative = (lSs.peek() == '-');
	if (lIsNextNumberNegative)
	{
		pLineString.erase(0, 1);
	}

	while (lSs >> i)
	{
		if (lSs.peek() == ',' || lSs.peek() == ' ')
		{
			// ignore that specific charachter
			lSs.ignore(1);
		}
		else if (lSs.peek() == ')')
		{
			// ignore next 3 charachters we know they are ") ("
			lSs.ignore(3);
		}
		else if (lSs.peek() == '-')
		{
			// we know next number is < 0
			lIsNextNumberNegative = true;
			lSs.ignore();
		}
		else if (lIsNextNumberNegative)
		{
			// we know next number > 0
			lIsNextNumberNegative = false;
		}

		// push either the positive number or the negative number into vector
		if (!lIsNextNumberNegative)
		{
			lToBeReturned.push_back(i);
		}
		else
		{
			lToBeReturned.push_back(-i);
		}

	}

	return lToBeReturned;
}

vector<float> UserInput::findFloatsInLineUser(string pLineString)
{
	// transform string into local stringstream
	stringstream lStream(pLineString);

	// vector to be returned
	vector<float> lToBeReturned;

	// parse string to get 4 ints following the following convention:
	// (INT1, INT2)(INT3, INT4)
	size_t lFirstBracketIndex = pLineString.find('(');
	pLineString = pLineString.erase(0, lFirstBracketIndex + 1); // it now is in the form we want it to be: INT1, INT2)(INT3, INT4)

	std::stringstream lSs(pLineString);

	float i;
	bool lIsNextNumberNegative = (lSs.peek() == '-');
	if (lIsNextNumberNegative)
	{
		pLineString.erase(0, 1);
	}

	while (lSs >> i)
	{
		if (lSs.peek() == ',' || lSs.peek() == ' ')
		{
			// ignore that specific charachter
			lSs.ignore(1);
		}
		else if (lSs.peek() == ')')
		{
			// ignore next 3 charachters we know they are ") ("
			lSs.ignore(3);
		}
		else if (lSs.peek() == '-')
		{
			// we know next number is < 0
			lIsNextNumberNegative = true;
			lSs.ignore();
		}
		else if (lIsNextNumberNegative)
		{
			// we know next number > 0
			lIsNextNumberNegative = false;
		}

		// push either the positive number or the negative number into vector
		if (!lIsNextNumberNegative)
		{
			lToBeReturned.push_back(i);
		}
		else
		{
			lToBeReturned.push_back(-i);
		}

	}

	return lToBeReturned;
}

void UserInput::redrawScene(vector<myPolygon> gDrawnPolys)
{
	// delete old scene
	std::fill(gPixelBuffer, gPixelBuffer + (gScreenHeight * gScreenWidth), 0);

	// find masterDenom, look through both lines and polygons
	float lMasterDenom = 0;

	float lMasterMin = gDrawnPolys[0].getVector()[1];
	float lMasterMax = gDrawnPolys[0].getVector()[1];

	vector<int> lTemp;

	for (int i = 0; i < gDrawnPolys.size(); i++)
	{
		lTemp = gDrawnPolys[i].getVector();
		for (int j = 1; j < lTemp.size(); j++)
		{
			if (lTemp[j] > lMasterMax)
			{
				lMasterMax = lTemp[j];
			}
			else if (lTemp[j] < lMasterMin)
			{
				lMasterMin = lTemp[j];
			}
		}
	}

	lMasterDenom = lMasterMax - lMasterMin;

	assert(lMasterDenom != 0);

	vector<int> lNew;

	for (int i = 0; i < gDrawnPolys.size(); i++)
	{
		lNew.clear();

		// push back the size
		lNew.push_back(gDrawnPolys[i].getVector()[0]);
		for (int j = 1; j < gDrawnPolys[i].getVector().size(); j += 2)
		{
			float lNormalizedX = (gDrawnPolys[i].getVector()[j] - lMasterMin) / lMasterDenom;
			float lNormalizedY = (gDrawnPolys[i].getVector()[j + 1] - lMasterMin) / lMasterDenom;
			lNew.push_back((int)(lNormalizedX * (gScreenHeight - 1)));
			lNew.push_back((int)(lNormalizedY * (gScreenWidth - 1)));
		}

		// set the draw vector
		gDrawnPolys[i].setDrawVector(lNew);
	}

	// fill PB again
	for (int i = 0; i < gDrawnPolys.size(); i++)
	{
		gDrawnPolys[i].drawPolygon();
	}
}