// library includes
#include <GL/glut.h>
#include <iostream>

// my includes
#include "Utility.h" // this header has all globals and all useful functions
#include "FileHandler.h"
#include "Polygon.h"
#include "UserInput.h"

// ADD TO MAKE FILE!!!!
#include "Line.h"

// function prototypes
void display();

UserInput gUI;
//vector keeping track of all drawn polygons
vector<myPolygon> gDrawnPolys;

vector<myPolygon> gCopyOfDrawnPolys;

vector<myLine> gLines;

int main(int argc, char *argv[])
{
	// just to test something unrelated to this project
	GLuint i = 0;

	// get user input to complete setup process
	gUI.displaySetupInput();

	// delete previous array
	delete[] gPixelBuffer;
	// create new array with correct dimensions
	gPixelBuffer = new float[gScreenHeight * gScreenWidth * 3];

	// initialization of pixelBuffer
	for (int i = 0; i < gScreenHeight * gScreenWidth * 3; i++)
	{
		gPixelBuffer[i] = 0;
	}

	// open and read the file
	FileHandler test("Input.txt");

	// interpret the file and execute all the drawing into the PixelBuffer
	test.interpretFile(gDrawnPolys, gLines);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE);

	//set window size
	glutInitWindowSize(gScreenHeight, gScreenWidth);

	//set window position
	glutInitWindowPosition(0, 0);

	//create and set main window title
	int MainWindow = glutCreateWindow("Hello Graphics!!");
	glClearColor(0, 0, 0, 0); //clears the buffer of OpenGL
							  //sets display function
	glutDisplayFunc(display);

	glutMainLoop(); // main display loop, will display until terminate
	return 0;
}

//main display loop, this function will be called again and again by OpenGL
void display()
{
	// THIS IS A TOTAL HACK!!!!!
	bool lNotDone = true;

	while (lNotDone)
	{
		//Misc.
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
	
		//fill the buffer
		gUI.redrawScene(gDrawnPolys);

		//draws pixel on screen, width and height must match pixel buffer dimension
		glDrawPixels(gScreenHeight, gScreenWidth, GL_RGB, GL_FLOAT, gPixelBuffer);
	
		//window refresh
		glEnd();
		glFlush();
		gUI.displayInteractionInput(gDrawnPolys, gLines);
	}
}