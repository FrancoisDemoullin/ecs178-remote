#pragma once

#include "cmath" // for basic functionality only, the only functions I use are abs() and floor()
#include "Utility.h"

using namespace std;

class myLine
{
public:
	myLine(int pA, int pB, int pC, int pD, char* pMode, float pRed, float pGreen, float pBlue);
	int a, b, c, d;
	void inline draw()
	{
		drawLineBresenham(a, b, c, d, 1, 1, 1);
	}

private:
	void drawLineBresenham(int pA, int pB, int pC, int pD, float pRed, float pGreen, float pBlue);
	void drawLineDDA(int pA, int pB, int pC, int pD, float pRed, float pGreen, float pBlue);
};