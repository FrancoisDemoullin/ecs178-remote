#include "Bspline.h"
#include <assert.h>

myBspline::myBspline(vector<int> pContolPoints, int pK, float pPrecision)
{
	pK = gK;
	pPrecision = gPrecision;

	float lInterpolatedValue = 0;
	float lToBeReturnedX = 0;
	float lToBeReturnedY = 0;

	int lN = pContolPoints[0] - 1;

	// get the normalized knot vector goes from [0..1]
	vector<float> lKnots = computeKnots(lN, pK);
	vector<int> lToBeReturned;
	lToBeReturned.push_back(0);

	for (float lT = 0; lT <= 1; lT += pPrecision)
	{
		// transform lT from [0..1] into [k-1..n+1]
		float lRangedT = lT * ((lN + 1) - (pK - 1)) + pK - 1;

		assert(pK - 1 <= lRangedT && lRangedT <= lN + 1);

		// reset
		lToBeReturnedX = 0;
		lToBeReturnedY = 0;

		for (int i = 1; i <= lN + 1; i++)
		{
			// this is the recursive function that adds up the tree we saw in class
			lInterpolatedValue = calculateWeightForPointI(i - 1 , pK, lN + 1, lRangedT, lKnots);
			if (lInterpolatedValue != 0) 
			{
				int a = 0;
			}
			lToBeReturnedX += lInterpolatedValue * pContolPoints[(i - 1) * 2 + 1]; // do the conversion to my syntax for indecies
			lToBeReturnedY += lInterpolatedValue * pContolPoints[(i - 1) * 2 + 2];
		}
		lToBeReturned.push_back(lToBeReturnedX); lToBeReturned.push_back(lToBeReturnedY);
		lToBeReturned[0] = (lToBeReturned.size() - 1) / 2;
	}

	mPointsOnLine = lToBeReturned;
}

float myBspline::calculateWeightForPointI(int pKnotIndex, int pK, int pNumberOfControlPoints, float pT, vector<float> pKnots)
{

	// base case
	if (pK == 1)
	{
		// check if pOrder is in between the 2 knots
		if (pT >= pKnots[pKnotIndex] && pT < pKnots[pKnotIndex + 1])
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	// recursion (the tree from class)
	// apply the formula

	float lNumerator1 = pT - pKnots[pKnotIndex];
	float lDenominator1 = pKnots[pKnotIndex + pK - 1] - pKnots[pKnotIndex];
	float lNumerator2 = pKnots[pKnotIndex + pK] - pT;
	float lDenominator2 = pKnots[pKnotIndex + pK] - pKnots[pKnotIndex + 1];

	float lSubweight1 = 0;
	float lSubweight2 = 0;

	if (lDenominator1 != 0)
	{
		lSubweight1 = lNumerator1 / lDenominator1 * calculateWeightForPointI(pKnotIndex, pK - 1, pNumberOfControlPoints, pT, pKnots);
	}
	if (lDenominator2 != 0)
	{
		lSubweight2 = lNumerator2 / lDenominator2 * calculateWeightForPointI(pKnotIndex + 1, pK - 1, pNumberOfControlPoints, pT, pKnots);
	}

	return lSubweight1 + lSubweight2;

}

vector<float> myBspline::computeKnots(int pN, int pOrder)
{
	vector<float> lKnots;
	for (int i = 0; i <= pN + pOrder + 1; i++) 
	{
		lKnots.push_back(i);
	}
	return lKnots;
}

vector<int> myBspline::computePoints(vector<int> pControlPoints, float pPrecision)
{
	vector<int> a;
	return a;
}