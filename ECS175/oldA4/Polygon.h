#pragma once

#include <vector>

#include "Utility.h"
#include "Line.h"
#include "Bezier.h"

using namespace std;

class myPolygon
{
public:

	inline myPolygon()
	{
		mId = -1;
	}
	inline myPolygon(vector<int> pV, int pID)
	{
		mPolygonVector = pV;
		mId = pID;
	}

	// constructor
	myPolygon(vector<int> pV);

	myPolygon(vector<int> pV, bool pIsControlPoly);

	// boolean const
	myPolygon(vector<int> pV, bool pIsControlPoly, bool pIsBezier, bool pIsThreaded, bool pIsBspline);
	
	// copy constructor needed to plug Polys into Vectors
	myPolygon(const myPolygon& other);


	inline vector<int> getVector()
	{
		return mPolygonVector;
	}

	inline void setVector(const vector<int>& pPolygonVector)
	{
		mPolygonVector = pPolygonVector;
	}

	inline void setDrawVector(const vector<int>& pDrawVector)
	{
		mDrawVector = pDrawVector;
	}

	inline bool getIsControlPoly()
	{
		return mIsControlPoly;
	}

	inline void setIsBezier(bool pBool) { mIsBezier = pBool; }
	inline void setIsBspline(bool pBool) { mIsBspline = pBool; }

	inline bool getIsBezier() { return mIsBezier; }

	// drawing the polygon
	void drawPolygon();

private:
	// attributes
	int mId;
	vector<int> mPolygonVector;
	bool mIsControlPoly;
	bool mIsBezier;
	bool mIsBspline;
	bool mIsThreaded;
	vector<int> mDrawVector;

	void removeDuplicates(vector<int>& pV);

	// helper functions to find max and min in corners
	int inline findMaxY(vector<int> pV)
	{
		vector<int> lAllYs;
		for (int i = 2; i < pV.size(); i = i + 2) {
			lAllYs.push_back(pV[i]);
		}
		return max_element(lAllYs.begin(), lAllYs.end())[0];
	}

	int inline findMinY(vector<int> pV)
	{
		vector<int> lAllYs;
		for (int i = 2; i < pV.size(); i = i + 2) {
			lAllYs.push_back(pV[i]);
		}
		return min_element(lAllYs.begin(), lAllYs.end())[0];
	}

	int inline findMaxX(vector<int> pV)
	{
		vector<int> lAllXs;
		for (int i = 1; i < pV.size(); i = i + 2) {
			lAllXs.push_back(pV[i]);
		}
		return max_element(lAllXs.begin(), lAllXs.end())[0];
	}

	int inline findMinX(vector<int> pV)
	{
		vector<int> lAllXs;
		for (int i = 1; i < pV.size(); i = i + 2) {
			lAllXs.push_back(pV[i]);
		}
		return min_element(lAllXs.begin(), lAllXs.end())[0];
	}
};