#pragma once

#include <vector>
#include "Polygon.h"

using namespace std;


class myBspline
{
public:
	myBspline(vector<int> pControlPoints, int pOrder, float pPrecision);

	// pControlPoints
	vector<int> computePoints(vector<int> pControlPoints, float pPrecision);

	void inline displayPolygon()
	{
		myPolygon a(mPointsOnLine, false, false, false, true);
		a.drawPolygon();
	}

private:
	vector<int> mControlPoints;
	float mPrecision;
	vector<int> mPointsOnLine;

	vector<float> computeKnots(int pN, int pOrder);
	float calculateWeightForPointI(int i, int pOrder, int pNumberOfControlPoints, float pT, vector<float> pKnots);
};