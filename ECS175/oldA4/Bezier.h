#pragma once

#include <vector>
#include "Polygon.h"

using namespace std;

 
class myBezier
{
public:
	myBezier(vector<int> pControlPoints, float pPrecision);

	// pControlPoints
	vector<int> computePoints(vector<int> pControlPoints, float pPrecision);
	void displayPolygon();
	vector<int> computePointsThreaded(vector<int> pControlPoints, float pPrecision);

private:
	vector<int> mControlPoints;
	float mPrecision;
	vector<int> mPointsOnLine;

	
};
