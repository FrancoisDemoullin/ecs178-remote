
#include "Polygon.h"
#include <algorithm> // only used to find max and min in vector
#include "Bspline.h"


myPolygon::myPolygon(vector<int> pV)
{
	mId = gIdIndex + 1;
	mPolygonVector = pV;
	
	mIsControlPoly = false;
	mIsBezier = false;
	mIsBspline = false;
	mIsThreaded = false;

	drawPolygon();
}

myPolygon::myPolygon(const myPolygon& other)
{
	mPolygonVector = other.mPolygonVector;
	mId = other.mId;
	mIsControlPoly = other.mIsControlPoly;
	mIsBezier = other.mIsBezier;
	mIsBspline = other.mIsBspline;
	mIsThreaded = other.mIsThreaded;
}

myPolygon::myPolygon(vector<int> pV, bool pIsControlPoly, bool pIsBezier, bool pIsThreaded, bool pIsBspline)
{
	if (pIsControlPoly)
	{
		mId = gIdIndex + 1;
	}
	mPolygonVector = pV;

	mIsControlPoly = pIsControlPoly;
	mIsBezier = pIsBezier;
	mIsBspline = pIsBspline;
	mIsThreaded = pIsThreaded;

	//drawPolygon();
}

myPolygon::myPolygon(vector<int> pV, bool pIsControlPoly)
{
	if (pIsControlPoly)
	{
		mId = gIdIndex + 1;
	}
	mPolygonVector = pV;

	mIsControlPoly = pIsControlPoly;
}

void myPolygon::drawPolygon()
{
	//draw the lines
	// Important: pV's firt element is the number of POINTS that constitute the polygon

	// This code is so bad it hurts my eyes
	// The deadline is tonight though and I have to finfish this, so I ll keep it!
	if (mDrawVector.size() == 0)
	{
		if (mIsControlPoly == false)
		{
			mDrawVector = mPolygonVector;
		}
		else
		{
			return;
		}
	}

	float lRed;
	float lGreen;
	float lBlue;

	if (mIsControlPoly)
	{
		// control polys in white
		lRed = 1;
		lGreen = 1;
		lBlue = 1;

	}
	
	else if (mIsBezier)
	{
		// Bezier in red
		lRed = 1;
		lGreen = 0;
		lBlue = 0;
	}
	else if (mIsBspline)
	{
		// bSpline in blue
		lRed = 0;
		lGreen = 0;
		lBlue = 1;
	}

	if (mDrawVector[0] >= 0)
	{
		for (int i = 0; i < mDrawVector[0]; i++)
		{
			// computing the indices for the line drawing
			int lIndex1 = 2 * i + 1;
			int lIndex2 = 2 * i + 2;
			int lIndex3;
			int lIndex4;
			if (i != mDrawVector[0] - 1)
			{
				lIndex3 = 2 * i + 3;
				lIndex4 = 2 * i + 4;
			}
			else
			{
				break;
			}

			// draw the line
			myLine a(mDrawVector[lIndex1], mDrawVector[lIndex2], mDrawVector[lIndex3], mDrawVector[lIndex4], "D", lRed, lGreen, lBlue);
		}
	}

	if (mIsControlPoly)
	{
		if (mIsBezier)
		{
			myBezier lBezier(mDrawVector, gPrecision);
			if (mIsThreaded)
			{
				lBezier.computePointsThreaded(mDrawVector, gPrecision);
			}
			else
			{
				lBezier.computePoints(mDrawVector, gPrecision);
			}
			
			lBezier.displayPolygon();
		}
		else if (mIsBspline)
		{
			myBspline lBspline(mDrawVector, gK, gPrecision);
			lBspline.computePoints(mDrawVector, gK);
			lBspline.displayPolygon();
		}
	}
}

void myPolygon::removeDuplicates(vector<int>& pV)
{
	for (int it = 1; it < pV.size() - 1; it += 1)
	{
		if (pV[it] == pV[it + 1] + 1)
		{
			pV.erase(pV.begin() + it);
			//pV.shrink_to_fit();
		}
	}
}


