#include "Bezier.h"
#include "Utility.h"
#include <assert.h>

#include <thread>

myBezier::myBezier(vector<int> pControlPoints, float pPrecision)
{
	pPrecision = gPrecision;

	mControlPoints = pControlPoints;
	// erase the first element which only holds the number of points
	mControlPoints.erase(mControlPoints.begin());
	mPrecision = pPrecision;
}


vector<int> myBezier::computePoints(vector<int> pControlPoints, float pPrecision)
{
	pPrecision = gPrecision;

	// variable definitons
	int lPoint1X;
	int lPoint1Y;
	int lPoint2X;
	int lPoint2Y;

	int lInterpolatedX;
	int lInterpolatedY;

	//vector<float> lToBeReturned;
	vector<float> lIterationHelper;

	//iterator
	double lT;

	// here is the double for loop! Only its a boule for loop with a while loop inside
	for (lT = 0; lT <= 1; lT += pPrecision)
	{
		// initally, all the points are present
		vector<float> lToBeReturned(pControlPoints.begin() + 1, pControlPoints.end());

		while (lToBeReturned.size() > 2)
		{
			lIterationHelper.clear();

			// go over the size of the current vector
			for (int i = 0; i <= lToBeReturned.size() - 4; i += 2)
			{
				lPoint1X = lToBeReturned[i];
				lPoint1Y = lToBeReturned[i + 1];
				lPoint2X = lToBeReturned[i + 2];
				lPoint2Y = lToBeReturned[i + 3];

				lInterpolatedX = gLerp(lPoint1X, lPoint2X, lT);
				lIterationHelper.push_back(lInterpolatedX);
				lInterpolatedY = gLerp(lPoint1Y, lPoint2Y, lT);
				lIterationHelper.push_back(lInterpolatedY);
			}

			lToBeReturned = lIterationHelper;
		}

		//the point has been computed, its the only element of lToBeReturned
		assert(lToBeReturned.size() == 2);
		mPointsOnLine.push_back(lToBeReturned[0]);
		mPointsOnLine.push_back(lToBeReturned[1]);
	}

	if (lT != 1)
	{
		mPointsOnLine.push_back(pControlPoints[pControlPoints.size() - 2]);
		mPointsOnLine.push_back(pControlPoints[pControlPoints.size() - 1]);
	}

	assert(mPointsOnLine.size() % 2 == 0);
	mPointsOnLine.insert(mPointsOnLine.begin(), mPointsOnLine.size() / 2);

	// start points have to be equal
	assert(mPointsOnLine[1] == pControlPoints[1] && mPointsOnLine[2] == pControlPoints[2]);
	// end points have to be equal
	assert(mPointsOnLine[mPointsOnLine.size() - 2] == pControlPoints[pControlPoints.size() - 2] && mPointsOnLine[mPointsOnLine.size() - 1] == pControlPoints[pControlPoints.size() - 1]);

	return mPointsOnLine;
}

void threadFunction(float pOne, float pTwo, float *pMemLocation, float pT)
{
	*pMemLocation = gLerp(pOne, pTwo, pT);
}


vector<int> myBezier::computePointsThreaded(vector<int> pControlPoints, float pPrecision)
{
	pPrecision = gPrecision;

	// variable definitons
	int lPoint1X;
	int lPoint1Y;
	int lPoint2X;
	int lPoint2Y;

	int lInterpolatedX;
	int lInterpolatedY;

	//iterator
	double lT;

	// here is the double for loop! Only its a double for loop with a while loop inside
	for (lT = 0; lT <= 1; lT += pPrecision)
	{
		// initally, all the points are present
		vector<float> lToBeReturned(pControlPoints.begin() + 1, pControlPoints.end());
		vector<float> lIterationHelper(pControlPoints.begin() + 1, pControlPoints.end());
		vector<std::thread> lAllThreads(pControlPoints.size());

		int lControlPointIndex = lToBeReturned.size();
		while (lControlPointIndex > 2)
		{

			// go over the size of the current vector
			int lIndex = 0;
			for (int i = 0; i <= lToBeReturned.size() - 4; i += 2)
			{
				// creat all the threads
				// pas values to be interpolated as well as the memory location to be written to
				lAllThreads.at(i) = std::thread(threadFunction, lToBeReturned[i], lToBeReturned[i + 2], &lIterationHelper[lIndex], lT);
				lAllThreads.at(i+1) = std::thread(threadFunction, lToBeReturned[i + 1], lToBeReturned[i + 3], &lIterationHelper[lIndex + 1], lT);
				lIndex += 2;	
			}

			// join all the threads
			for (int k = 0; k < lAllThreads.size(); k++)
			{
				if (lAllThreads[k].joinable())
				{
					lAllThreads[k].join();
				}
			}
			lToBeReturned = lIterationHelper;
			lControlPointIndex -= 2;
		}

		//the point has been computed, its the only element of lToBeReturned
		//assert(lToBeReturned.size() == 2);
		mPointsOnLine.push_back(lToBeReturned[0]);
		mPointsOnLine.push_back(lToBeReturned[1]);
	}

	if (lT != 1)
	{
		mPointsOnLine.push_back(pControlPoints[pControlPoints.size() - 2]);
		mPointsOnLine.push_back(pControlPoints[pControlPoints.size() - 1]);
	}

	assert(mPointsOnLine.size() % 2 == 0);
	mPointsOnLine.insert(mPointsOnLine.begin(), mPointsOnLine.size() / 2);

	// start points have to be equal
	assert(mPointsOnLine[1] == pControlPoints[1] && mPointsOnLine[2] == pControlPoints[2]);
	// end points have to be equal
	assert(mPointsOnLine[mPointsOnLine.size() - 2] == pControlPoints[pControlPoints.size() - 2] && mPointsOnLine[mPointsOnLine.size() - 1] == pControlPoints[pControlPoints.size() - 1]);

	return mPointsOnLine;
}

void myBezier::displayPolygon()
{
	myPolygon a(mPointsOnLine, false, true, false, false);
	a.drawPolygon();
}

