#pragma once

// library includes
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

// my includes
#include "Utility.h"

class UserInput
{
public:

	// TODO make static at some point (better coding practice)

	// methods
	void displaySetupInput();
	void displayInteractionInput(vector<myPolygon>& gDrawnPolys, vector<myLine>& gLines);


	void redrawScene(vector<myPolygon> gDrawnPolys);

private:
	void interpretUserInput(string pInputString, vector<myPolygon>& gDrawnPolys, vector<myLine>& gLines);

	vector<int> findIntsInLineUser(string pLineString);
	vector<float> findFloatsInLineUser(string pLineString);

};