
#include "FileHandler.h"

FileHandler::FileHandler(const char* pFilePath)
{
	
	// vector containing each line of the file
	vector<string> lFileLines;

	// string containing the currentLine
	string lCurrentLineString;

	//File handler
	ifstream lFileStream(pFilePath);

	// ensures ifstream objects can throw exceptions
	lFileStream.exceptions(ifstream::badbit); // | ifstream::failbit 

	try
	{
		// loop through all the lines in the file
		while (getline(lFileStream, lCurrentLineString))
		{
			// push currentLine into vector
			lFileLines.push_back(lCurrentLineString);
		}

	}
	catch (ifstream::failure e)
	{
		cout << "ERROR::File::FILE_NOT_SUCCESFULLY_READ. PATH: vert:  + " << e.what() << " " << pFilePath << endl;
	}

	// finally save the local file string into the private member variable of the class by calling the setter
	FileHandler::setFileLines(lFileLines);
}

void FileHandler::interpretFile(vector<myPolygon>& pP, vector<myLine>& pL)
{
	// local copy of vector
	vector<string> lFileLines = FileHandler::getFileLines();

	// iterate over vector
	for (vector<string>::iterator it = lFileLines.begin(); it != lFileLines.end(); ++it)
	{
		// call private function that interprets every line
		FileHandler::interpretLine(*it, pP, pL);
	}
}

void FileHandler::interpretLine(string pLineString, vector<myPolygon>& pP, vector<myLine>& pL)
{
	
	if (pLineString.find("Bresenham") != std::string::npos)
	{
		vector<int> lIntsInLine = FileHandler::findIntsInLine(pLineString);

		// drawing the line
		pL.push_back( myLine(lIntsInLine[0], lIntsInLine[1], lIntsInLine[2], lIntsInLine[3], "B", 1, 1, 1) );
	}
	else if (pLineString.find("DDA") != std::string::npos)
	{
		vector<int> lIntsInLine = FileHandler::findIntsInLine(pLineString);

		// draw the line
		pL.push_back( myLine(lIntsInLine[0], lIntsInLine[1], lIntsInLine[2], lIntsInLine[3], "D", 1, 1, 1) );
	}
	else if (pLineString.find("Polygon") != std::string::npos)
	{
		vector<int> lIntsInLine = FileHandler::findIntsInLine(pLineString);

		// add polygon to vector, these are all control polys, so set flag to true
		

		if (pLineString.find("Bezier") != std::string::npos)
		{
			if (pLineString.find("threaded") != std::string::npos)
			{
				pP.push_back(myPolygon(lIntsInLine, true, true, true, false));
			}
			else
			{
				pP.push_back(myPolygon(lIntsInLine, true, true, false, false));
			}
		}
		else if (pLineString.find("Bspline") != std::string::npos)
		{
			pP.push_back(myPolygon(lIntsInLine, true, false, false, true));
		}
		else
		{
			std::cout << "Something is wrong with your input" << std::endl;
		}
	}
	else
	{
		cout << "no operation corresponding to: " << pLineString << " found!" << endl;
	}
}

vector<int> FileHandler::findIntsInLine(string pLineString)
{
	// transform string into local stringstream
	stringstream lStream(pLineString);

	// vector to be returned
	vector<int> lToBeReturned;
	
	// parse string to get 4 ints following the following convention:
	// (INT1, INT2)(INT3, INT4)
	size_t lFirstBracketIndex = pLineString.find('(');
	pLineString = pLineString.erase(0, lFirstBracketIndex + 1); // it now is in the form we want it to be: INT1, INT2)(INT3, INT4)

	std::stringstream lSs(pLineString);

	int i;
	bool lIsNextNumberNegative = (lSs.peek() == '-');
	if (lIsNextNumberNegative)
	{
		pLineString.erase(0, 1);
	}

	while (lSs >> i)
	{
		if (lSs.peek() == ',' || lSs.peek() == ' ')
		{
			// ignore that specific charachter
			lSs.ignore(1);
		}
		else if (lSs.peek() == ')')
		{
			// ignore next 3 charachters we know they are ") ("
			lSs.ignore(3);
		}
		else if (lSs.peek() == '-')
		{
			// we know next number is < 0
			lIsNextNumberNegative = true;
			lSs.ignore();
		}
		else if( lIsNextNumberNegative)
		{
			// we know next number > 0
			lIsNextNumberNegative = false;
		}

		// push either the positive number or the negative number into vector
		if (!lIsNextNumberNegative)
		{
			lToBeReturned.push_back(i);
		}
		else
		{
			lToBeReturned.push_back(-i);
		}
			
	}

	return lToBeReturned;
}