#pragma once

#include <vector>
#include <algorithm>
#include <iostream>


#include "Polygon.h"

// global variables
extern float* gPixelBuffer;
extern int gScreenWidth;
extern int gScreenHeight;
extern int gIdIndex;
extern int gLineIdIndex;
extern bool gTerminate;

extern float gPrecision;
extern int gK;

// useful functions
void inline gMakePixel(int pX, int pY)
{
	int pixel = 3 * (pX + (pY * gScreenWidth) ); // gScreenWidth
	for (int i = 0; i < 3; i++)
	{
		// turn on the RGB value
		gPixelBuffer[pixel + i] = 1;
	}
}

bool inline gCheckPixel(int pX, int pY)
{
	int pixel = 3 * (pX + (pY * gScreenWidth));
	return (gPixelBuffer[pixel] == 1);
}

void inline gPrintSeperatorToConsole()
{
	std::cout << "===================================" << std::endl;
}

float inline gLerp(float a, float b, float f)
{
	return a + f * (b - a);
}

void inline gMakePixelColor(int pX, int pY, float pRed, float pGreen, float pBlue)
{
	int pixel = 3 * (pX + (pY * (gScreenWidth)));

	// set RGB to corresponding values
	gPixelBuffer[pixel] = pRed;
	gPixelBuffer[pixel + 1] = pGreen;
	gPixelBuffer[pixel + 2] = pBlue;
}