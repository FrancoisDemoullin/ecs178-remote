#include "Utility.h"

float* gPixelBuffer = new float[0];
int gScreenWidth = 0;
int gScreenHeight = 0;

bool gTerminate = false;

float gPrecision = 0.005;
int gK = 3;

// keeps track of ID index
int gIdIndex = 0;
int gLineIdIndex = 0;
