

Input:



You can enter polygons only using the Input.txt file.
A sample file with sample input is provided. Do not change the directory of that file and please do not change the name of it either.

You can enter Bezier Polygons as follows:
Polygon Bezier (4) (10, 10) (50, 50) (70, 70) (90, 10)

This will compute a polygon with 4 control points (hence the first number in brackets)
The algorithm used is the DeCasteljau algo we have seen in class.

You can have Bezier curves computed with threads (Read the next section on threads) by typing:
Polygon Bezier threaded (4) (10, 10) (50, 50) (70, 70) (90, 10)

I warn you, performance will be slow.

You can enter Bspline control Polygons as follows:
Polygon Bspline (4) (10, 10) (50, 50) (70, 70) (90, 10)


Once your input file is all set up, run the program. You will be prompted to input t (I called it precision) and k. 
These values are uniform for all polygons in the scene. I was simply too lazy to implement it otherwise. Sorry.
Thus make sure your k value is smaller than the smallest Bspline control poly. 



UI:


Once that is all done, you can see your happy polygons. 
A UI will be displayed in the command line (Use alt + tab to switch to it if you cannot see it right away).
The UI lets you input a bunch of stuff, use alt + tab after you validate your input to go back to the drawn polygons.

First of all, type info.
It shows all the polygons in the scene and it lays out their indecies. The first polygon from your file has index 1, the second polygon has index 2 etc... 
It even displays fancy lines to show sections, isnt that cool!?

The UI lets you add points as follows
add (1) (2) (400, 400)

Here the polygonIndex is 1
The index at which you want to enter the point is 2. That means that the new point will be at index 2 of the control points. 
If you want to add a point to the beginning of the polygon, the index you need to put is 0.
If you want to add a point at the end of the polygon, the index is the number of control points of the poly + 1. Say there are 4 control points for the polygon, your index will be 5 if you want to add a point at the end.

You can remove points:
remove (2) (1)

This will remove point 1 from polygon with id 2
Again, if you are confused about ids and about what polygons have what points, type info. It displays all the polys and it helps you adding and deleting points.

You can change the value of points too:
change (2) (2) (400, 400)

You see how it works now, they all follow the same pattern...

Type save and your polys will be saved (Though information about threading will be lost but threading in this case was a bad idea in the first place so that is actually a pretty good feature).
They will be saved to Output.txt (copy and paste from Output.txt to Input.txt if you want to see that the scene is still the same)

You can change the precision of the polygons, and the k for all bspline curves. 
	
What you cannot do is add entire polygons on the fly, however if you are willing to understand my reasoning behind it, read the following, if not, jump to the next section:

Let me ask you something. How is this relevant to the course material?
I fully understand that program syntax is important and following the instructions on an assignment sheet is at the very basis of the idea behind school. But really? You think it is relevant to add an entire 
polygons on the fly is something I absolutely need to have implemented before I can successfully complete ECS175? How is adding a polygon in the command line even practical. It is a huge pain in the ass!
Ok, so if you want to add a new polygon, here is how its done:
Add the Polygon to Input.txt using the handy syntax I describe in my readme.
Terminate the program
Re-compile
There you go, there is your polygon, much easier than me prompting you for a bunch of numbers.
I must admit, I personally think the way this class is taught discourages people from learning Computer Graphics. In an entire quarter we have not talked once about the graphics pipeline. We haven't even touched shaders,
We have no idea about the different shader stages or the graphics pipeline. Which is undoubtedly at the very basis of Computer Graphics. I understand that handling the pixel buffer is interesting and useful. But really? 5 assignments and one entire quarter on just the pixel buffer?
People in the industry dont even know what the pixel buffer is. Nobody uses it. It is too low level. But not even low level enough to do some serious optimizations on it. 
Instead of teaching us the basics of Computer Graphics you make us implement totally useless commands using the command line. Add a polygon on the fly, change add and delete a point. I mean why the hell would you make us do all that?
I spent more time implementing silly commands than actually implementing relevant algorithms that relate to Computer Graphics.
This classes teaches me very little about Computer Graphics and I regret having taken it. 
As a side note, every time I ask Bernd about anything that is not related to the course material I get an odd look, some comment that makes fun of me and I am being told to talk to someone else. Hopefully that will be reflected in the teaching evaluations and I am sure it will.
That does not apply to Michael (who is seeing this in the first place because its an even number assignment and I know Michael gets the beginning of the alphabet), Michael is a great TA.
That is my personal opinion and I felt like expressing myself. 

Regardless. Deduct marks if you want to, after all this is a requirement which I am just too lazy to implement. However, I gave instructions on how to add a polygon. My method actually has an easier syntax than what you require me to do, so I even save you time.
So if that reflection makes sense to you, do not deduct marks.



Threading:



Abstract

The idea was to implement the de Casteljau algorithm using Threads. In theory it makes sense, for each iteration of the algorithm I use threads to find the correct interpolated value.
I then use those values and re-use threads to compute the newly interpolated values of the new generation. 
This promises to make change the algorithm with time complexity O(n^2) to an algorithm of complexity O(n).
Think about it, I would get rid of an entire for loop  iteration. However I would still need to loop because I have to open the threads. In theory, it would be O(n) though. 

Method

I used std::threads. They are part of the standard template C++ library. The integration with my program was easy and std::threads all me to cross compile them on my windows platform as well as on the CSIF machines at school.
The implementation of the De Casteljau algortihm was tedious, with certain parts breaking at multiple places. (In other words, it took me ages to get it running! What a waste of time...)
In the end, the breakup of the threaded algorithm is very very similar to the non threaded version, I needed one helper function that was passed into the thread.

Algorithm

You can find the algorithm in Bezier.cpp from line 93-166.
The idea was to launch a high number of threads that concurrently find the interpolated value for each segment. 
Then, join all the threads into the main threads. 
This required me to pass a pointer to a memory location to each thread function. The thread would then write the newly computed value into the specified memory location, before returning. 
For each interpolation two threads are launched, one for the x interpolation and one for the y interpolation. this could have been combined into one single thread. However the anture of this project was to illustrate that concurrent interpolation yields in a speed-up so I chase 
to dispatch as many threads as I could.

Results

I tested my threaded version on the following control polygon:

Polygon (40) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100) (100, 100) (400, 100) (400, 400) (100, 400) (100, 100)
(Please, do not run this yourself, unless you feel like making the CSIF machines crash)

The compile time for the threaded algorithm was around 40 minutes, compared to a minimal 0.5 seconds for the non-threaded version.
Even a much smaller polygon with 5 or 6 control points will make the threaded version take significantly longer than the non threaded version. With the threaded version having a speed-up of -200% on average at a 4 point polygon.
The speed up being negative disproves my hypothesis of making the De Casteljau algorithm faster. 

Discussion:

Why are the results this terrible? Here are some reasons:
1. std::threads are BAD! I knew that from the get go. The good compatibility and the easiness to implement them made me choose them. A much smarter version should have implemented using pThreads, CUDA or OpenCL (That would require GPU context switches though).
2. Creating a thread and joining it has overhead
3. Each thread goes off to compute a single line of code
4. Each interpolated value requires a thread for x and a thread for y values
5. I am not using a cutoff
A cutoff would be really nice here. The idea would be that when there are lots of values to be computed I would use threads, however once we get to a smaller number of values that need to be computed I would use the regular implementation.

Conclusion.
I was a bad idea from the get go!
However it was more fun than implementing silly user input commands that have nothing to do with computer graphics.





Known Bugs



I am making you life easy this time. 
Instead of having you actively look for bugs I tell you what is wrong. 
If you change the precision using the UI, for some reason, the old Bspline curves will still be visible. I am not too sure why. But then again I have spent way too much time on this so I don't feel like looking any closer
You cannot enter entire polygons on the fly
You cannot change the type of a curve on the fly. Say you cannot make a Bezier cure be a Bspline and vice versa. (Not too sure if that needed to be done actually... In case it did, I am sorry)



Inventory of my code

De Casteljau: Bezier.cpp 18-84
Threaded de Casteljau: Bezier.cpp 93-166 Please consider the helper functions: threadFunction in Bezier.cpp 87-91 and gLerp in Utility.h 43-46
Bspline: Bspline.cpp the entire class is relevant. Just look at the constructor and then the helper functions which are called from there. 
 

 
 Thank you for reading all of this. 